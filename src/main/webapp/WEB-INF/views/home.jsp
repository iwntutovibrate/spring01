<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
	<title>home</title>
	<meta charset="UTF-8"/>
	<script src="/spring01/res/js/jquery-3.3.1.min.js"></script>
	<script src="/spring01/res/js/sockjs-0.3.4.js"></script>
</head>
<style>
#contents{
	margin: auto;
	margin-top: 150px;
	width: 500px;
	height: 500px;
	background-color: gray;
}
#message{
	position: relative;
	margin-top : 440px;
	margin-left : 30px;
	height: 50px;
	width: 400px;
	border-radius: 15px;
	background-color: gold;
}

#chatForm{
	position: relative;
	top: -430px;
}

#chat{
	overflow-y: auto; 
	height:500px;
}
</style>
<body>
	<input type="hidden" id="nick" value="${nick}">
	<div id="contents">
		<div id="chat"></div>
		<form id="chatForm">
			<input type="text" id="message"/>
			<button>send</button>
		</form>
	</div>

</body>
<script>
	$(document).ready(function(){
		$("#chatForm").submit(function(event){
			event.preventDefault();
			sock.send($("#message").val());
			$("#message").val('').focus();
			
			// 스크롤 하단 이동
			$("#chat").scrollTop($(document).height());
		});
		
	});
	
	var sock = new SockJS("/spring01/echo");
	sock.onmessage = function(e){
		var nickName = $('#nick').val();
		$("#chat").append(nickName + " : "+ e.data + "<br/>");
		
	}
	
	sock.onclose = function(){
		$("#chat").append("연결 종료");
	}
	
</script>
</html>